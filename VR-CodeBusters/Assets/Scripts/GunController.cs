using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    [SerializeField] private GameObject laser;
    [SerializeField] private GameObject gameControlGameObject;
    public GameObject camera;
    private LineRenderer lr;
    private AudioSource lrSource;
    private AudioSource lrSource2;
    public bool is_active = false;
    public bool active = false;
    private float time_power = 0f;
    private float max_time_power = 1f;
    private GameControl gameControl;


    void Start()
    {
        lr = laser.GetComponent<LineRenderer>();
        lrSource = laser.GetComponents<AudioSource>()[0];
        lrSource2 = laser.GetComponents<AudioSource>()[1];
        gameControl = gameControlGameObject.GetComponent<GameControl>();

    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            time_power = Mathf.Min(max_time_power, time_power + Time.deltaTime * 5);

            lr.startWidth = time_power;
            lr.endWidth = time_power;

            lrSource.mute = false;
            lr.SetPosition(0, laser.transform.position);
            RaycastHit hit;

            if (Physics.Raycast(laser.transform.position, laser.transform.forward, out hit))
            {
                if (hit.collider)
                {
                    if (hit.collider.gameObject.layer == 7)
                    {
                        gameControl.KillOne();
                        Destroy(hit.collider.gameObject);
                    }
                    lr.SetPosition(1, hit.point);
                }
            }
            else
            {
                lr.SetPosition(1, transform.forward * 700);
            }
        } else
        {
            time_power = 0f;
            lrSource.mute = true;
            lr.SetPosition(0, laser.transform.position);
            lr.SetPosition(1, laser.transform.position);
        }

        if (is_active)
        {
            transform.rotation = camera.transform.rotation;
            lrSource2.mute = false;
        } else
        {
            lrSource.mute = true;
            lrSource2.mute = true;
            transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
        }
    }
}
