using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestroy : MonoBehaviour
{
    [SerializeField] private float lifeTime;
    [SerializeField] private Light light;

    private float time;

    void Update()
    {
        time += Time.deltaTime;
        light.intensity = 5f*(1f - time/lifeTime);
        if (time > lifeTime)
        {
            Destroy(gameObject);
        }
    }
}
