using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    [SerializeField] private GameObject effect;
    [SerializeField] private GameObject meteorHole;
    [SerializeField] private float speed = 10f; 
    private float timeDelivery;
    private float time;
    private Vector3 startPostion;
    void Start()
    {
        startPostion = transform.position;
        timeDelivery = Vector3.Distance(Vector3.zero, transform.position) / speed;
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        transform.position = Vector3.Lerp(startPostion,new Vector3(-2.6f, 0.5f, 4.5f), time / timeDelivery);
    }

    public void OnDestroy()
    {
        if (Vector3.Distance(Vector3.zero, transform.position) < 10f)
        {
            // if destroy in build
            Instantiate(effect, transform.position, transform.rotation);
            Instantiate(meteorHole, new Vector3(transform.position.x, -0.2f, transform.position.z), Quaternion.identity);
        }
        else
        {
            // if destroy in shield
            Instantiate(effect, transform.position, transform.rotation);
        }
    }
}
