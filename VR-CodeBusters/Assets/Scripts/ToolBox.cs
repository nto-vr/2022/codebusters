using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class ToolBox : MonoBehaviour
{
    public Text textObject;
    private List<GameObject> gameObjects = new List<GameObject>();
    private int endurance = 3;
    [SerializeField] private GameObject source;

    private void OnTriggerEnter(Collider other)
    {
        gameObjects.Add(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        gameObjects.Remove(other.gameObject);
    }

    void Start()
    {
        
    }

    void Update()
    {
        if (endurance == 3)
        {
            textObject.color = new Color(0f, 1f, 0f);
        } else if (endurance == 2)
        {
            textObject.color = new Color(1f, 1f, 0f);
        } else if (endurance == 1)
        {
            textObject.color = new Color(1f, 0f, 0f);
        }
        textObject.text = endurance.ToString();

        if (gameObject.layer == 8)
        {
            for (var i = gameObjects.Count - 1; i > -1; i--)
            {
                if (gameObjects[i] && gameObjects[i].layer == 12)
                {
                    if (gameObjects[i].transform.parent && gameObjects[i].transform.parent.GetComponent<Door>())
                    {
                        gameObjects[i].transform.parent.GetComponent<Door>().go_open();
                        endurance -= 1;
                        Instantiate(source, transform.position, transform.rotation);
                    }
                    else
                    {
                        endurance -= 1;
                        Destroy(gameObjects[i]);
                        gameObjects.Remove(gameObjects[i]);
                        Instantiate(source, transform.position, transform.rotation);
                    }
                }
                else
                {
                    gameObjects.Remove(gameObjects[i]);
                }
            }
        }

        if (endurance <= 0)
        {
            Destroy(gameObject);
        }
    }
}
