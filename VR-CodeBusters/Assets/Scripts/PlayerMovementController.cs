using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using UnityEngine.Events;

public class PlayerMovementController : MonoBehaviour
{
    public SteamVR_Action_Vector2 touchController;
    public SteamVR_Action_Boolean swithMovementType;
    public float speed = 1f;
    private bool can_switch = true;
    private bool is_teleporting = true;

    public UnityEvent enableTeleporting, disableTeleporting;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!is_teleporting)
        {
            Vector3 movementDir = Player.instance.hmdTransform.TransformDirection(new Vector3(touchController.axis.x, 0, touchController.axis.y));
            transform.position += Vector3.ProjectOnPlane(Time.deltaTime * movementDir * 2.0f, Vector3.up);
        }

        // Debug.Log(swithMovementType.activeDevice + " " + swithMovementType.state + " " + swithMovementType.stateUp);

        if (swithMovementType.state && can_switch)
        {
            can_switch = false;
            is_teleporting = !is_teleporting;
            // Debug.Log(is_teleporting);
            if (is_teleporting) { enableTeleporting.Invoke(); } else { disableTeleporting.Invoke(); }
        } else if (!swithMovementType.state)
        {
            can_switch = true;
        }
    }


    public void Success1()
    {
        Debug.Log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
    }

    public void Success2()
    {
        Debug.Log("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
    }
}
