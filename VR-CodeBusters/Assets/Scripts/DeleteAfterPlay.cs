using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteAfterPlay : MonoBehaviour
{
    private AudioSource source;
    
    void Start()
    {
        source = gameObject.GetComponent<AudioSource>();        
    }
    
    void Update()
    {
        if (!source.isPlaying)
        {
            Destroy(gameObject);
        }    
    }
}
