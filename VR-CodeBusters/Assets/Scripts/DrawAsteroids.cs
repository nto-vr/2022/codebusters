using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrawAsteroids : MonoBehaviour
{
    [SerializeField] private List<GameObject> images = new List<GameObject>();
    [SerializeField] private List<GameObject> crashImages = new List<GameObject>();
    [SerializeField] private GameObject gameControlGameObject;
    [SerializeField] private Text text;

    private GameControl gameControl;
    private float zoom = 0.4f;
    private float minDist;
    
    void Start()
    {
        gameControl = gameControlGameObject.GetComponent<GameControl>();
    }

    
    void Update()
    {
        minDist = 10000f;
        if (gameControl.GetHp() < 90)
        {
            foreach (var image in crashImages) 
            {
                image.SetActive(true);
            }
        }
        else
        {
            foreach (var image in crashImages) 
            {
                image.SetActive(false);
            }
        }
        for (var i = 0; i < 10; i++)
        {
            images[i].SetActive(false);
            if (i < gameControl.meteors.Count)
            {
                if (Vector3.Distance(gameControl.meteors[i].transform.position, Vector3.zero) < minDist)
                {
                    minDist = Vector3.Distance(gameControl.meteors[i].transform.position, Vector3.zero);
                }

                if (gameControl.GetHp() >= 90)
                {
                    images[i].SetActive(true);                    
                }
                images[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(gameControl.meteors[gameControl.meteors.Count - i - 1].transform.position.x * zoom + 642,
                    -gameControl.meteors[gameControl.meteors.Count - i - 1].transform.position.z * zoom - 346);
            }
        }
        
        if (minDist == 10000f)
        {
            minDist = 0;
        }
        text.text = $"MIN DIST:   {(int)minDist}\nTARGET:    {gameControl.meteors.Count}\nKILLED: {gameControl.GetKilled()}";
    }
}
