using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.PlayerLoop;

public class Buzzer : MonoBehaviour
{
    [SerializeField] private AudioClip impact;
    [SerializeField] private float deltaTime = 1f;
    [SerializeField] private float volume = 0.5f;
    
    private AudioSource sound;
    private bool isOn;
    private float time;

    private void Start()
    {
        sound = GetComponent<AudioSource>();
    }

    private void Update()
    {
        time += Time.deltaTime;
        if (isOn && time >= deltaTime)
        {
            // Debug.Log("PLAY");
            sound.PlayOneShot(impact, volume);
            time = 0;
        }
    }

    public void OnBuzzer()
    {
        if (!isOn)
        {
            time = deltaTime + 1f;   
        }
        isOn = true;
    }

    public void OffBuzzer()
    {
        isOn = false;
    }
}
