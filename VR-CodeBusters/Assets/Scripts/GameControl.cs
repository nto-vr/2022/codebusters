using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameControl : MonoBehaviour
{
    [SerializeField] private float charge = 0.75f;
    [SerializeField] private int hp = 100;
    [SerializeField] private float doorChargePerSecond = 0.01f;
    [SerializeField] private float lampChargePerSecond = 0.01f;
    [SerializeField] private float minChargeLamp = 0.1f;
    [SerializeField] private float minChargeDoor = 0.2f;
    [SerializeField] private float minChargeGun = 0.3f;
    [SerializeField] private float minChargeYellow = 0.3f;
    [SerializeField] private float minChargeGreen = 0.75f;
    [SerializeField] private List<Door> doors = new List<Door>();
    [SerializeField] private List<StationarLampController> lamps = new List<StationarLampController>();
    [SerializeField] private Buzzer buz;
    [SerializeField] private LampController _light_shoot;
    [SerializeField] private LampController charge_lamp;
    [SerializeField] private List<GameObject> meteor_prefabs = new List<GameObject>();
    [SerializeField] private List<GameObject> end_explosions = new List<GameObject>();
    [SerializeField] private GameObject shield;
    [SerializeField] private float deltaDeliverysTime = -3f;
    [SerializeField] private float distance = 500f;
    [SerializeField] private float timeFromLastDanger = 5f;
    [SerializeField] private GameObject gameOwer;
    public List<GameObject> meteors = new List<GameObject>();
    private bool onLamps = true;
    private bool onDoors = true;
    private bool isAlarm;
    private float timeLastDanger;
    private int killed;
    private bool isRestart;
    private bool isOffBlink;
    private float timeBlink;

    private void Update()
    {
        if (hp == 0 && !isRestart)
        {
            isRestart = true;
            gameOwer.SetActive(true);
        }

        if (hp == 0 && meteors.Count == 0)
        {
            GameObject.FindGameObjectWithTag("11111111").transform.position = new Vector3(-100, 100, 100);
            StartCoroutine(LoadAsync("Level"));
            return;
        }
        
        if (timeLastDanger > 0 && charge > 0 && hp >= 30)
        {        
            timeLastDanger -= Time.deltaTime;
            shield.SetActive(true);
        }
        else
        {
            shield.SetActive(false);
        }
        
        if (deltaDeliverysTime < 0)
        {
            deltaDeliverysTime += Time.deltaTime;
        }
        else if (deltaDeliverysTime < 1f && !isRestart)
        {
            
            var count = Random.Range(5, 10);
            for (var i = 0; i < count; i++)
            {
                var az = Mathf.Deg2Rad *  Random.Range(-180.0f, 180.0f);
                var el = Mathf.Deg2Rad * Random.Range(10f, 40f);
                var dist = distance + Random.Range(0f, 300f);
                var x = dist * Mathf.Cos(el) * Math.Cos(az);
                var y = dist * Mathf.Sin(el);
                var z = dist * Mathf.Cos(el) * Math.Sin(az);
                var meteor = meteor_prefabs[Random.Range(0, meteor_prefabs.Count)];
                meteor.transform.localScale = new Vector3(Random.Range(3, 11), Random.Range(3, 11), Random.Range(3, 11));
                meteors.Add(Instantiate(meteor, new Vector3((float)x, y, (float)z), Quaternion.identity));                    
            }

            deltaDeliverysTime = 10f;
        }
        else
        {
            for (var i = meteors.Count - 1; i > -1; i--)
            {
                if (!meteors[i])
                {
                    meteors.RemoveAt(i);
                    continue;
                }

                if (Vector3.Distance(new Vector3(-2.6f, 0.5f, 4.5f), meteors[i].transform.position) < 30f)
                {
                    timeLastDanger = timeFromLastDanger;
                }
                if (Vector3.Distance(new Vector3(-2.6f, 0.5f, 4.5f), meteors[i].transform.position) < 3f)
                {
                    Destroy(meteors[i]);
                    meteors.Remove(meteors[i]);
                    DownHP(10);
                }
                else if (charge > 0 && hp >= 30 && Vector3.Distance(new Vector3(-2.6f, 0.5f, 4.5f), meteors[i].transform.position) < 25f)
                {
                    Destroy(meteors[i]);
                    meteors.Remove(meteors[i]);
                    DownCharge(0.1f);
                }
            }
            if (meteors.Count == 0)
            {
                deltaDeliverysTime = Random.Range(-25f, -15f);
            }
        }

        isAlarm = deltaDeliverysTime > -2;
        
        if (onDoors)
        {
            charge -= doorChargePerSecond * Time.deltaTime;       
        }
        
        if (onLamps)
        {
            charge -= lampChargePerSecond * Time.deltaTime;
        }

        if (charge < 0)
        {
            charge = 0;
        }
        
        if (charge > minChargeGun)
        {
            _light_shoot.setGreenColor();
        } else
        {
            _light_shoot.setRedColor();
        }

        if (charge > minChargeGreen)
        {
            charge_lamp.setGreenColor();
        } else if (charge > minChargeYellow)
        {
            charge_lamp.setYellowColor();
        } else
        {
            charge_lamp.setRedColor();
        }

        if (charge < minChargeDoor && onDoors)
        {
            foreach (var door in doors)
            {
                door.go_close();
            }
            onDoors = false;
        }
        else if (charge > minChargeDoor && hp < 30)
        {
            if (doors[0].IsOpen())
            {
                foreach (var door in doors)
                {
                    door.go_close();
                }
                onDoors = false;
            }
            else if (doors[0].IsClose())
            {
                foreach (var door in doors)
                {
                    door.go_open();
                }
                onDoors = true;
            }
        }
        else if (charge > minChargeDoor && !onDoors)
        {
            foreach (var door in doors)
            {
                door.go_open();
            }
            onDoors = true;
        }
        
        if (charge < minChargeLamp && onLamps)
        {
            foreach (var lamp in lamps)
            {
                lamp.PowerOff();
            }
            onLamps = false;
        }
        else if (charge > minChargeLamp && hp < 70)
        {
            timeBlink -= Time.deltaTime;
            if (timeBlink < 0)
            {
                timeBlink = Random.Range(0f, 2f);
                isOffBlink = !isOffBlink;
            }
            if (isOffBlink)
            {
                foreach (var lamp in lamps)
                {
                    lamp.PowerOff();
                }
            }
            else
            {
                foreach (var lamp in lamps)
                {
                    lamp.PowerOn();
                }
            }
        }
        else if (charge > minChargeLamp && !onLamps)
        {
            foreach (var lamp in lamps)
            {
                lamp.PowerOn();
            }
            onLamps = true;
        }

        if (isAlarm)
        {
            buz.OnBuzzer();
        }
        else
        {
            buz.OffBuzzer();
        }

        foreach (var lamp in lamps)
        {
            if (isAlarm || hp < 30)
                lamp.ALARM();
            else
                lamp.NORMAL();
        }
    }
    
    public float GetCharge()
    {
        return charge;
    }

    public void UpCharge(float delta)
    {
        charge += delta;
        if (charge > 1f)
        {
            charge = 1f;
        }
    }

    public void DownCharge(float delta)
    {
        charge -= delta;
        if (charge < 0f)
        {
            charge = 0f;
        }
    }

    public float GetHp()
    {
        return hp;
    }

    public void UpHP(int delta)
    {
        hp += delta;
        if (hp > 100)
        {
            hp = 100;
        }
    }
    
    public void DownHP(int delta)
    {
        hp -= delta;
        if (hp < 0)
        {
            hp = 0;
        }
    }

    public void OnAlarm()
    {
        isAlarm = true;
    }
    
    public void OffAlarm()
    {
        isAlarm = false;
    }
    
    public void SomeProblem()
    {
        charge = 0;
    }
    
    IEnumerator LoadAsync(string name)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(name);
        yield return null;
    }

    public void KillOne()
    {
        killed++;
    }

    public int GetKilled()
    {
        return killed;
    }
}
