using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationarLampController : MonoBehaviour
{
    public Light light1;
    public Light light2;
    public Light light3;
    public Light light4;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    private void setInten(float inten)
    {
        light1.intensity = inten;
        light2.intensity = inten;
        light3.intensity = inten;
        light4.intensity = inten;
    }

    private void setCol(float r, float g, float b)
    {
        light1.color = new Color(r, g, b);
        light2.color = new Color(r, g, b);
        light3.color = new Color(r, g, b);
        light4.color = new Color(r, g, b);
    }

    public void PowerOn()
    {
        setInten(0.9f);
    }

    public void PowerOff()
    {
        setInten(0f);
    }

    public void ALARM()
    {
        setCol(1f, 0f, 0f);
    }

    public void NORMAL()
    {
        setCol(1f, 1f, 1f);
    }
}