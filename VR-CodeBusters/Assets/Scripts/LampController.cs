using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampController : MonoBehaviour
{

    public Light _light;
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void setGreenColor()
    {
        _light.color = new Color(0f, 1f, 0f);
    }

    public void setYellowColor()
    {
        _light.color = new Color(1f, 1f, 0f);
    }

    public void setRedColor()
    {
        _light.color = new Color(1f, 0f, 0f);
    }
}
