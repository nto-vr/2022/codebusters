using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroyAllMeteors : MonoBehaviour
{
    [SerializeField] private LampController active_lamp;
    [SerializeField] private GameControl gameControl;
    [SerializeField] private GameObject laser;
    [SerializeField] private float timeBetweenShot;
    [SerializeField] private float drawLaserTime;
    private LineRenderer lr;
    private AudioSource lrSource;
    private bool active = false;
    private float timerBetween;
    private Vector3 oldForward = Vector3.forward;

    void Start()
    {
        lr = laser.GetComponent<LineRenderer>();
        lrSource = gameObject.GetComponent<AudioSource>();
        lrSource.mute = true;
    }

    void Update()
    {
        lrSource.mute = true;
        
        lr.SetPosition(0, laser.transform.position);
        lr.SetPosition(1, laser.transform.position);


        if (active && gameControl.meteors.Count > 0 && gameControl.GetHp() >= 70)
        {
            if (gameControl.GetCharge() < 0.3f)
            {
                changeActive();
            }

            gameControl.DownCharge(0.01f * Time.deltaTime);

            timerBetween += Time.deltaTime;

            if (timerBetween < 0)
            {
                lr.SetPosition(0, laser.transform.position);
                lr.SetPosition(1, transform.forward * 700);   
            }
            else if (timerBetween < timeBetweenShot)
            {
                transform.LookAt(gameControl.meteors[0].transform.position);
                transform.forward = Vector3.Lerp(oldForward, transform.forward, timerBetween / timeBetweenShot);
            }
            else
            {
                lrSource.mute = false;

                transform.LookAt(gameControl.meteors[0].transform.position);

                Destroy(gameControl.meteors[0]);
                gameControl.meteors.RemoveAt(0);
                gameControl.KillOne();

                gameControl.DownCharge(0.06f);

                lr.SetPosition(0, laser.transform.position);
                lr.SetPosition(1, transform.forward * 700);

                timerBetween = -drawLaserTime;

                oldForward = transform.forward;
            }
        }
        /*

        if (gameControl.meteors.Count > 0 && timerBetween <= 0)
        {
            timerBetween = 1f;
            transform.LookAt(gameControl.meteors[0].transform.position);
            Destroy(gameControl.meteors[0]);
            gameControl.meteors.RemoveAt(0);
            gameControl.DownCharge(0.06f);
        }

        timerBetween -= Time.deltaTime;

        if (timerBetween > 0)
        {
            lrSource.mute = false;
            float func_znach = timerBetween;

            lr.startWidth = func_znach;
            lr.endWidth = func_znach;

            lr.SetPosition(0, laser.transform.position);
            lr.SetPosition(1, transform.forward * 700);
        } else
        {
            lrSource.mute = true;
        }
    } else
    {
        lrSource.mute = true;
        timerBetween = 0f;
    }
    */
    }

    public void changeActive()
    {
        if (active || gameControl.GetCharge() > 0.3f)
        {
            active = !active;
            if (active)
            {
                active_lamp.setGreenColor();
            } else
            {
                active_lamp.setRedColor();
            }
        }
    }
}
