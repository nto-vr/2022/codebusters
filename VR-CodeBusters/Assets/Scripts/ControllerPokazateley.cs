using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerPokazateley : MonoBehaviour
{
    [SerializeField] private GameControl gameControl;
    [SerializeField] private Image imageHp;
    [SerializeField] private Image imageCharge;
    [SerializeField] private Text textHp;
    [SerializeField] private Text textCharge;


    void Start()
    {
        
    }

    void Update()
    {
        int hp = (int)gameControl.GetHp();
        imageHp.fillAmount = hp / 100f;
        imageCharge.fillAmount = gameControl.GetCharge();
        textHp.text = hp.ToString();
        textCharge.text = ((int)(gameControl.GetCharge() * 100)).ToString();
        imageHp.color = new Color((hp > 50 ? ((100 - hp) / 50f) : 1), (hp < 50 ? (hp / 50f) : 1), 0f);
    }
}
