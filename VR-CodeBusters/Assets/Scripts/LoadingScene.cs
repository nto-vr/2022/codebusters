using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScene : MonoBehaviour
{
    public string sceneName;
    
    public float time=10;
    
    public Slider bar;

    private bool canOpen;
    private bool loading = false;

    public void Start()
    {
        Load();
    }

    public void Update()
    {
        if (loading)
        {
            if (time > 0) time -= Time.deltaTime;
            if (time <= 0) canOpen = true;   
            bar.value = 1 - time / 10;
        }
    }

    public void Load()
    {
        // SceneManager.LoadScene(sceneName);
        loading = true;
        StartCoroutine(LoadAsync());
        // StartCoroutine(ExecuteAfterTime(10));
    }
    
    IEnumerator LoadAsync()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        asyncLoad.allowSceneActivation = false;
        while (!asyncLoad.isDone)
        {
            if (canOpen)
            {
                asyncLoad.allowSceneActivation = true;
            }
            yield return null;
        }
    }
    IEnumerator ExecuteAfterTime(float timeInSec)
    {
        yield return new WaitForSeconds(timeInSec);
        canOpen = true;
    }
}
