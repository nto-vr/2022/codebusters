using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KillerYbivcaSmerti : MonoBehaviour
{
    private List<GameObject> gameObjects = new List<GameObject>();
    public string message = "";
    public GameControl gameControl;

    public UnityEvent i_eat;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (message != "")
        {
            Debug.Log(message);
            message = "";
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        gameObjects.Add(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        gameObjects.Remove(other.gameObject);
    }

    public void Kill()
    {
         // Debug.Log("? _ ?");
        foreach (var gameObject in gameObjects)
        {
            if (gameObject.layer == 9 && gameControl.GetCharge() < 0.9f)
            {
                gameObjects.Remove(gameObject);
                Destroy(gameObject);
                i_eat.Invoke();
                break;
            }
        }
    }
}
