using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{

    [SerializeField] private Transform leftDoor;
    [SerializeField] private Transform rightDoor;

    private bool state = true;
    private bool is_opening;
    private bool is_closing;
    private bool isClose;
    private bool isOpen = true;
    private float time;
    private float animationTime = 1f;

    private Vector3 leftClose = new Vector3(0, 1.3f, -0.42f);
    private Vector3 rightClose = new Vector3(0, 1.3f, 0.33f);

    private Vector3 leftOpen = new Vector3(0, 1.3f, -1.8f);
    private Vector3 rightOpen = new Vector3(0, 1.3f, 1.5f);

    private void Update()
    {
        time += Time.deltaTime;
        var progress = time / animationTime;

        if (is_opening)
        {
            if (progress > 1)
            {
                leftDoor.localPosition = leftOpen;
                rightDoor.localPosition = rightOpen;
                is_opening = false;
                isOpen = true;
            }
            else
            {
                leftDoor.localPosition = Vector3.Lerp(leftClose, leftOpen, progress);
                rightDoor.localPosition = Vector3.Lerp(rightClose, rightOpen, progress);
                isOpen = false;
            }
        }
        else if (is_closing)
        {
            if (progress > 1)
            {
                leftDoor.localPosition = leftClose;
                rightDoor.localPosition = rightClose;
                is_closing = false;
                isClose = true;
            }
            else
            {
                leftDoor.localPosition = Vector3.Lerp(leftOpen, leftClose, progress);
                rightDoor.localPosition = Vector3.Lerp(rightOpen, rightClose, progress);
                isClose = false;
            }
        }
    }

    public void go_open()
    {
        isOpen = false;
        isClose = false;
        if (!state)
        {
            if (is_closing)
            {
                time = animationTime - time;
            }
            else if (is_opening)
            {

            }
            else
            {
                time = 0;
            }

            is_opening = true;
            is_closing = false;
            state = true;
            leftDoor.gameObject.layer = 0;
            rightDoor.gameObject.layer = 0;
        }
    }

    public bool IsOpen()
    {
        return isOpen;
    }
    
    public bool IsClose()
    {
        return isClose;
    }
    
    public bool IsIdle()
    {
        return !is_closing && !is_opening;
    }
    
    public void go_close()
    {
        isOpen = false;
        isClose = false;
        if (state)
        {
            if (is_opening)
            {
                time = animationTime - time;

            }
            else if (is_closing)
            {

            }
            else
            {
                time = 0;
            }
            is_opening = false;
            is_closing = true;
            state = false;
            leftDoor.gameObject.layer = 12;
            rightDoor.gameObject.layer = 12;
        }
    }
}