using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.UI;
using UnityEngine.Events;

public class Periscope : MonoBehaviour
{
    public GameObject core;
    public GameControl gameControl;
    public SteamVR_Action_Boolean Shoot;
    public SteamVR_Action_Boolean LeaveGun;
    public Light _light;
    public Material m_Material;
    public Camera TheCamera;
    public Camera AddCamera;
    private float timer = 0f;
    private bool power_on = false;
    private bool work = false;
    private bool power_off = false;
    public Text textObject;
    private float power_off_time = 3f;
    private float power_on_time = 4f;
    private float gun_rotation_speed = 3000f;
    public GunController gunController;

    public UnityEvent can_work, full_charge, start_work, end_work;

    void Start()
    {
        
    }

    public void ChangeCamera()
    {
        if (gameControl.GetCharge() > 0.5f && !gunController.is_active)
        {
            timer = 0f;
            TheCamera.enabled = !TheCamera.enabled;
            AddCamera.enabled = !AddCamera.enabled;
            power_on = true;
            gunController.is_active = true;
            start_work.Invoke();
        }
    }

    void Update()
    {
        if (power_on)
        {
            _light.intensity = Mathf.Min(timer, 4f);
            core.transform.Rotate(0f, core.transform.rotation.y + Time.deltaTime * Mathf.Min(gun_rotation_speed * timer / power_on_time, gun_rotation_speed), 0f);
            m_Material.color = new Color(Mathf.Min(1f * (timer) / power_on_time, 0.8f), 0f, 0f);
            textObject.text = $"������� {(int)(gameControl.GetCharge() * 100)}%";

            timer += Time.deltaTime;
            if (timer > power_on_time)
            {
                power_on = false;
                work = true;
                timer = 0f;
            }
        } else if (work)
        {
            core.transform.Rotate(0f, core.transform.rotation.y + Time.deltaTime * gun_rotation_speed, 0f);
            _light.intensity = Mathf.Min(4f);
            m_Material.color = new Color((power_off_time - timer) / power_off_time, 0f, 0f);

            textObject.text = $"������� {(int)(gameControl.GetCharge() * 100)}%";

            if (gameControl.GetCharge() < 0.3f || LeaveGun.state) {
                work = false;
                power_off = true;
            } else if (Shoot.state)
            {
                gunController.active = true;
                gameControl.DownCharge(0.05f * Time.deltaTime);
            } else
            {
                gunController.active = false;
            }
        }
        else if (power_off)
        {
            gunController.active = false;
            timer += Time.deltaTime;

            _light.intensity = Mathf.Min((power_off_time - timer) / power_off_time * 4f, 4f);
            m_Material.color = new Color((power_off_time - timer) / power_off_time, 0f, 0f);
            core.transform.Rotate(0f, core.transform.rotation.y + gun_rotation_speed * Time.deltaTime * Mathf.Max(0f, (power_off_time - timer) / power_off_time), 0f);

            textObject.text = $"������� {(int)(gameControl.GetCharge() * 100)}%";

            if (timer > power_off_time)
            {
                gunController.is_active = false;
                power_off = false;
                TheCamera.enabled = !TheCamera.enabled;
                AddCamera.enabled = !AddCamera.enabled;
                end_work.Invoke();
            }
        }
        else
        {
            _light.intensity = 0f;
            textObject.text = "";
        }
    }
}
