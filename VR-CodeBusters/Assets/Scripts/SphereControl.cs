using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereControl : MonoBehaviour
{
    private GameControl gameControl;
    private float damage_timer;

    void Start()  // GameControl
    {
        gameControl = GameObject.FindGameObjectWithTag("GameControl").GetComponent<GameControl>();
    }

    void Update()
    {
        damage_timer += Time.deltaTime;
        if (damage_timer > 3f)
        {
            damage_timer = 0f;
            gameControl.DownHP(1);
        }
    }

    void OnDestroy()
    {
        gameControl.UpHP(7);
    }
}
