using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesManger : MonoBehaviour
{
    public string menuSceneName;
    public string gameSceneName;

    IEnumerator LoadAsync(string name)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(name);
        yield return null;
    }

    public void Restart()
    {
        StartCoroutine(LoadAsync(gameSceneName));
    }

    public void ToMainMenu()
    {
        StartCoroutine(LoadAsync(menuSceneName));
    }

    public void GoQuit()
    {
        Application.Quit();
    }
}
