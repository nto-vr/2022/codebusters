using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shadows : MonoBehaviour
{
    [SerializeField] private float animationTime;
    [SerializeField] private float muteTime;

    private float time;
    [SerializeField] private Image image;

    private void Start()
    {
        image = gameObject.GetComponent<Image>();
    }

    private void Update()
    {
        time += Time.deltaTime;
        if (time < animationTime * 2)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, 1 - time / (animationTime * 2));
        }
        else if (time > muteTime)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, (time - muteTime) / animationTime);
        }
        else
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, 0f);
        }
    }
}
